package utils;

public class GlobalConstants {

    // EXCEPTION MESSAGES
    public static final String PRIMARY_KEY_NOT_FOUND = "The provided class: ( %s ) doesn't have a primary key!";
    public static final String ENTITY_NOT_FOUND_ANNOTATION = "The provided class: ( %s ) cannot be persisted!";
    public static final String ENTITY_NOT_FOUND_BY_ID = "Entity with id: ( %d ) not found!";
    public static final String ENTITY_DUPLICATION = "Entity with id: ( %d ) already exists!";
    public static final String ENTITY_INVALID = "Entity should contain valid id: ( %d )!";
    public static final String ENTITY_INVALID_CREATE = """
            Entity should not contain an id: ( %d ) when persisted for the first time!
            The database is generating an id for every new entity.
            """;

    // SQL QUERIES
    public static final String SELECT_ALL_FROM_QUERY = "SELECT * FROM %s";
    public static final String SELECT_ALL_FROM_WHERE_QUERY = "SELECT * FROM %s WHERE %s";
    public static final String INSERT_INTO_QUERY = "INSERT INTO %s (%s) VALUES (%s)";
    public static final String UPDATE_ROW_QUERY = "UPDATE %s SET %s WHERE %s";
    public static final String DELETE_ROW_QUERY = "DELETE FROM %s WHERE id = %s";
    public static final String SELECT_TABLE_SCHEMA_QUERY = """
            SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_SCHEMA = DATABASE()
            AND TABLE_NAME = '%s';
            """;
    public static String CREATE_NEW_TABLE_QUERY = """
            CREATE TABLE %s
            (
                %s AUTO_INCREMENT PRIMARY KEY,
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
            );
            """;
}