import configuration.DatabaseConfiguration;
import database.HibernateImpl;
import database.contracts.Hibernate;
import exceptions.EntityNotFoundException;
import exceptions.IllegalEntityException;
import exceptions.PrimaryKeyNotFoundException;
import factories.CountryFactory;
import models.entities.Country;
import models.entities.Department;
import models.entities.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

public class ApplicationStarter {

    public static void main(String[] args) throws SQLException {

        try (final Connection connection = DatabaseConfiguration.getConnection()) {
            Hibernate<User> userTable = new HibernateImpl<>(connection, User.class);
            Hibernate<Country> countryTable = new HibernateImpl<>(connection, Country.class);
            Hibernate<Department> departmentTable = new HibernateImpl<>(connection, Department.class);
            userTable.getAll().forEach(System.out::println);
            System.out.println("----------------");
            userTable.getAll(" first_name LIKE '%g%'").forEach(System.out::println);
            System.out.println("----------------");
            countryTable.getAll().forEach(System.out::println);
            System.out.println("----------------");
            System.out.println(userTable.getById(4));
            System.out.println("----------------");
//            System.out.println(userTable.create(UserFactory.createUserWithoutId()));
//            System.out.println("----------------");
            //System.out.println(countryTable.create(CountryFactory.createCountryWithoutId()));
            System.out.println("----------------");
//            User user = UserFactory.createUserWithId();
//            user.setId(20);
//            user.setFirstName("Doncho");
//            user.setLastName("Donchev");
//            user.setAge(32);
//            System.out.println(userTable.update(user));
//            System.out.println("----------------");
//            Country country = CountryFactory.createCountryWithId();
//            country.setName("USA");
//            System.out.println(countryTable.update(country));
//            System.out.println("----------------");
//            User user = UserFactory.createUserWithId();
//            user.setId(22);
//            System.out.println(userTable.delete(user));
            Country country = CountryFactory.createCountryWithId();
            System.out.println(countryTable.delete(country));
        } catch (EntityNotFoundException | PrimaryKeyNotFoundException | SQLSyntaxErrorException
                | IllegalAccessException | IllegalEntityException ex) {
            System.out.println(ex.getMessage());
        }
    }
}