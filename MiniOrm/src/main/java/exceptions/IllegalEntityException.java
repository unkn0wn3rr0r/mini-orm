package exceptions;

public class IllegalEntityException extends RuntimeException {

    public IllegalEntityException() {
        super();
    }

    public IllegalEntityException(String message) {
        super(message);
    }
}