package exceptions;

public class PrimaryKeyNotFoundException extends RuntimeException {

    public PrimaryKeyNotFoundException() {
        super();
    }

    public PrimaryKeyNotFoundException(String message) {
        super(message);
    }
}