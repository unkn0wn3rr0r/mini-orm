package exceptions;

public class EntityDuplicatedException extends RuntimeException {

    public EntityDuplicatedException() {
        super();
    }

    public EntityDuplicatedException(String message) {
        super(message);
    }
}