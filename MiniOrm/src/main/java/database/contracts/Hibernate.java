package database.contracts;

import java.sql.SQLException;
import java.util.List;

public interface Hibernate<T> {

    boolean create(T entity) throws IllegalAccessException, SQLException;

    boolean update(T entity) throws IllegalAccessException, SQLException;

    boolean delete(T entity) throws IllegalAccessException, SQLException;

    T getById(long id) throws SQLException;

    List<T> getAll() throws SQLException;

    List<T> getAll(String criteria) throws SQLException;
}