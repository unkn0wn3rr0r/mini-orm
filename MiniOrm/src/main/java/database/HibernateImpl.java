package database;

import annotations.Column;
import annotations.Entity;
import annotations.Id;
import annotations.Table;
import database.contracts.Hibernate;
import exceptions.EntityNotFoundException;
import exceptions.IllegalEntityException;
import exceptions.PrimaryKeyNotFoundException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static utils.GlobalConstants.*;

public class HibernateImpl<T> implements Hibernate<T> {

    private static final String TABLE_CREATION_SUCCESS = "Table: %s, created successfully!";
    private static final String TABLE_CREATION_FAIL = "%s Failed to create table: %s!";
    private static final String[] PLACEHOLDERS = {
            "%s %s",
            "%s",
            "%s = %s",
            "id = %d",
    };

    private final Connection connection;
    private final Class<T> entity;

    public HibernateImpl(Connection connection, Class<T> entity) {
        this.connection = connection;
        this.entity = entity;

        if (!tableExists()) {
            System.out.println(createTable());
        }
    }

    private String createTable() {
        List<String> sqlValues = new LinkedList<>();
        sqlValues.add(getTableName());

        for (Field field : getColumnFields()) {
            field.setAccessible(true);
            Column column = field.getAnnotation(Column.class);
            String columnName = column.name();
            String fieldName = getFieldNameForSqlQuery(field.getType().getSimpleName());
            String formatRowValues = format(PLACEHOLDERS[0], columnName, fieldName);
            sqlValues.add(formatRowValues);
        }

        CREATE_NEW_TABLE_QUERY = createNewTableQuery(sqlValues);

        try {
            executePreparedStatement(CREATE_NEW_TABLE_QUERY);
            return format(TABLE_CREATION_SUCCESS, getTableName());
        } catch (SQLException ex) {
            return format(TABLE_CREATION_FAIL, ex.getMessage(), getTableName());
        }
    }

    private String createNewTableQuery(List<String> sqlValues) {
        final String stringToReplace = PLACEHOLDERS[1];

        for (String sqlValue : sqlValues) {
            CREATE_NEW_TABLE_QUERY = CREATE_NEW_TABLE_QUERY.replaceFirst(stringToReplace, sqlValue);
        }
        // if there are unused `%s` left, replace them with empty string so we can execute the query
        return CREATE_NEW_TABLE_QUERY = CREATE_NEW_TABLE_QUERY.replaceAll(stringToReplace, "");
    }

    private String getFieldNameForSqlQuery(String fieldName) {
        return switch (fieldName) {
            case "int", "Integer", "double", "Double", "long", "Long" -> "INT";
            case "String" -> "VARCHAR(250)";
            default -> throw new IllegalStateException("Unexpected value: " + fieldName);
        };
    }

    @Override
    public boolean create(T entity) throws IllegalAccessException, SQLException {
        Field getPrimaryKeyField = getPrimaryKeyField();
        getPrimaryKeyField.setAccessible(true);

        long primaryKey = (long) getPrimaryKeyField.get(entity);
        if (primaryKey > 0) {
            throw new IllegalEntityException(format(ENTITY_INVALID_CREATE, primaryKey));
        }

        List<String> columnNames = new LinkedList<>();
        List<Object> columnValues = new LinkedList<>();
        fillColumnNamesAndValues(entity, columnNames, columnValues);

        String columns = String.join(", ", columnNames);
        String values = columnValues
                .stream()
                .map(obj -> obj instanceof String ? "'" + obj + "'" : obj)
                .map(String::valueOf)
                .collect(Collectors.joining(", "));

        final String sql = format(INSERT_INTO_QUERY, getTableName(), columns, values);
        return executePreparedStatement(sql);
    }

    private void fillColumnNamesAndValues(T entity, List<String> columnNames, List<Object> columnValues) throws IllegalAccessException {
        for (Field field : getColumnFields()) {
            field.setAccessible(true);
            Column column = field.getAnnotation(Column.class);
            String columnName = column.name();
            Object columnValue = field.get(entity);
            columnNames.add(columnName);
            columnValues.add(columnValue);
        }
    }

    @Override
    public boolean update(T entity) throws IllegalAccessException, SQLException {
        Field primaryKeyField = getPrimaryKeyField();
        primaryKeyField.setAccessible(true);

        throwIfInvalidPrimaryKey(entity, primaryKeyField);

        List<Field> columnFields = getColumnFields();
        List<String> processedFields = addFieldsFormattedToString(entity, columnFields);

        String valuesForTemplate = getFieldValuesToString(processedFields);

        final String sql = format(UPDATE_ROW_QUERY, getTableName(), valuesForTemplate, processedFields.get(0));
        return executePreparedStatement(sql);
    }

    private List<String> addFieldsFormattedToString(T entity, List<Field> columnFields) throws IllegalAccessException {
        List<String> processedFields = new ArrayList<>();

        for (Field field : columnFields) {
            field.setAccessible(true);
            Column columnField = field.getAnnotation(Column.class);
            String columnName = columnField.name();
            Object fieldValue = field.get(entity);
            String data = fieldValue instanceof String
                    ? format(PLACEHOLDERS[2], columnName, format("'%s'", fieldValue))
                    : format(PLACEHOLDERS[2], columnName, fieldValue);
            processedFields.add(data);
        }

        return processedFields;
    }

    private void throwIfInvalidPrimaryKey(T entity, Field primaryKeyField) throws IllegalAccessException {
        long primaryKey = (long) primaryKeyField.get(entity);
        if (primaryKey < 1) {
            throw new IllegalEntityException(format(ENTITY_INVALID, primaryKey));
        }
    }

    private String getFieldValuesToString(List<String> processedFields) {
        StringBuilder valuesForTemplate = new StringBuilder();

        for (int i = 1; i < processedFields.size(); i++) {
            String currentField = processedFields.get(i);
            if (i == processedFields.size() - 1) {
                valuesForTemplate.append(currentField);
            } else {
                valuesForTemplate.append(currentField).append(", ");
            }
        }

        return valuesForTemplate.toString();
    }

    @Override
    public boolean delete(T entity) throws IllegalAccessException, SQLException {
        Field primaryKeyField = getPrimaryKeyField();
        primaryKeyField.setAccessible(true);

        throwIfInvalidPrimaryKey(entity, primaryKeyField);

        long primaryKey = (long) primaryKeyField.get(entity);

        final String sql = format(DELETE_ROW_QUERY, getTableName(), primaryKey);
        return executePreparedStatement(sql);
    }

    @Override
    public T getById(long id) throws SQLException {
        final String tableName = getTableName();
        final String where = format(PLACEHOLDERS[3], id);
        final String sql = format(SELECT_ALL_FROM_WHERE_QUERY, tableName, where);

        ResultSet resultSet = getResultSet(sql);

        List<T> result = toList(resultSet);
        if (result.isEmpty()) {
            throw new EntityNotFoundException(format(ENTITY_NOT_FOUND_BY_ID, id));
        }

        return result.get(0);
    }

    @Override
    public List<T> getAll() throws SQLException {
        return getAll(null);
    }

    @Override
    public List<T> getAll(String criteria) throws SQLException {
        final String sql = criteria == null
                ? format(SELECT_ALL_FROM_QUERY, getTableName())
                : format(SELECT_ALL_FROM_WHERE_QUERY, getTableName(), criteria);

        ResultSet resultSet = getResultSet(sql);

        return Collections.unmodifiableList(toList(resultSet));
    }

    private List<T> toList(ResultSet resultSet) throws SQLException {
        List<T> result = new LinkedList<>();

        while (resultSet.next()) {
            T currentEntity = createNewEntity(resultSet);
            result.add(currentEntity);
        }

        return result;
    }

    private T createNewEntity(ResultSet resultSet) {
        T newEntity = null;

        try {
            newEntity = entity.getDeclaredConstructor().newInstance();
            Field primaryKey = getPrimaryKeyField();
            setPrimaryKeyToEntity(resultSet, newEntity, primaryKey);

            List<Field> allColumnFields = getColumnFields();
            for (Field field : allColumnFields) {
                Column column = field.getAnnotation(Column.class);
                String columnName = column.name();
                field.setAccessible(true);
                setEntityValue(resultSet, newEntity, field, columnName);
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException | SQLException e) {
            e.printStackTrace();
        }

        return newEntity;
    }

    private void setPrimaryKeyToEntity(ResultSet resultSet, T newEntity, Field primaryKey)
            throws SQLException, IllegalAccessException {

        Id idAnnotation = primaryKey.getAnnotation(Id.class);
        String idName = idAnnotation.annotationType().getSimpleName().toLowerCase();

        long idValue = resultSet.getLong(idName);
        primaryKey.setAccessible(true);
        primaryKey.set(newEntity, idValue);
    }

    private List<Field> getColumnFields() {
        return Arrays.stream(entity.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Column.class))
                .collect(Collectors.toList());
    }

    private Field getPrimaryKeyField() {
        return Arrays.stream(entity.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Id.class))
                .findFirst()
                .orElseThrow(() -> new PrimaryKeyNotFoundException(format(PRIMARY_KEY_NOT_FOUND, entity.getSimpleName())));
    }

    private void setEntityValue(ResultSet resultSet, T newEntity, Field field, String columnName)
            throws SQLException, IllegalAccessException {

        if (long.class.equals(field.getType()) || Long.class.equals(field.getType())) {
            long value = resultSet.getLong(columnName);
            field.set(newEntity, value);

        } else if (int.class.equals(field.getType()) || Integer.class.equals(field.getType())) {
            int value = resultSet.getInt(columnName);
            field.set(newEntity, value);

        } else if (String.class.equals(field.getType())) {
            String value = resultSet.getString(columnName);
            field.set(newEntity, value);
        }
    }

    private String getTableName() {
        if (!entity.isAnnotationPresent(Entity.class) && !entity.isAnnotationPresent(Table.class)
                || (!entity.isAnnotationPresent(Entity.class) || !entity.isAnnotationPresent(Table.class))) {
            throw new EntityNotFoundException(format(ENTITY_NOT_FOUND_ANNOTATION, entity.getSimpleName()));
        }

        Table table = entity.getAnnotation(Table.class);
        return table.name();
    }

    private boolean tableExists() {
        final String sql = format(SELECT_TABLE_SCHEMA_QUERY, getTableName());

        try {
            ResultSet resultSet = getResultSet(sql);
            return resultSet.next();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    private boolean executePreparedStatement(String sql) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        return preparedStatement.execute();
    }

    private ResultSet getResultSet(String sql) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        return preparedStatement.executeQuery();
    }
}