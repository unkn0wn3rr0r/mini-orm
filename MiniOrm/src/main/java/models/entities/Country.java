package models.entities;

import annotations.Column;
import annotations.Entity;
import annotations.Id;
import annotations.Table;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    public Country() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%d %s", id, name);
    }
}