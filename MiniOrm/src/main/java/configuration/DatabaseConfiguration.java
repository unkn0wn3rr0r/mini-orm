package configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConfiguration {

    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/orm_test_db";
    private static final String DATABASE_USERNAME = "root";
    private static final String DATABASE_PASSWORD = "12453";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                CONNECTION_STRING,
                DATABASE_USERNAME,
                DATABASE_PASSWORD
        );
    }
}