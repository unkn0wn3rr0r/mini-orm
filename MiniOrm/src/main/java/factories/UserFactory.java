package factories;

import models.entities.User;

public class UserFactory {

    private static final long ID = 1;
    private static final String FIRST_NAME = "Maria";
    private static final String LAST_NAME = "Ilieva";
    private static final int AGE = 25;

    public static User createUserWithoutId() {
        var user = new User();

        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setAge(AGE);

        return user;
    }

    public static User createUserWithId() {
        var user = new User();

        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setAge(AGE);

        return user;
    }
}