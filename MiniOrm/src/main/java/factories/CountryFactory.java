package factories;

import models.entities.Country;

public class CountryFactory {

    private static final long ID = 2;
    private static final String NAME = "Japan";

    public static Country createCountryWithoutId() {
        var country = new Country();

        country.setName(NAME);

        return country;
    }

    public static Country createCountryWithId() {
        var country = new Country();

        country.setId(ID);
        country.setName(NAME);

        return country;
    }
}