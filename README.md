# Mini-ORM

Custom _Hibernate_ like mini **O**bject **R**elational **M**apping framework.

### Author:

- _Kaloyan Stoyanov_

### Technologies used:

- _JDK 15_
- _IntelliJ IDEA_ - as an IDE during the development process
- _MySQL/MariaDB_ - as a database
- _MySQL Connector/J_ - JDBC Type 4 driver for MySQL
- _Gradle_ - as a build automation tool
- _Git_ - for source control management and documentation